require 'Math'
class RangeList 
  def initialize
    @rl = Array.new
  end

  def add(range)
    @rl << range
    @rl.sort!{|x,y| x[0] <=> y[0]} # 保证区间的左边界有序

    _tmp = Array.new
    @rl.each_with_index do |v,k|
      left,right = v[0],v[1]
      # 第一次遍历或区间无交集时直接赋值，否则交集内的右边界取最大值
      (_tmp.size == 0 || _tmp[-1][1] < left) ? _tmp << [left,right] : _tmp[-1][1] = [_tmp[-1][1],right].max
    end

    @rl = _tmp
  end 
  
  def remove(range)
    _r1 = (@rl[0][0]..@rl[-1][1])
    _r2 = (range[0]..range[1])

    if _r2.cover?(_r1) # 若range包含整个区间
      @rl = (_r1 == _r2) ? [[_r2.first,_r1.first],[_r2.last,_r2.last]] : [] # 相同时进行拆分，否则全部remove
      return
    end

    _tmp = Array.new
    @rl.each_with_index do |v,k| # 考虑range在某个区间的各种情况
      left,right = v[0],v[1]
      next if _r2.first < left && _r2.last > right # 1.range包含区间时

      if _r2.first > right || _r2.last < left # 2.range与区间无交集时
        _tmp << [left,right]
      elsif _r2.first > left && _r2.last < right # 3.range在区间内时
        _tmp << [left,_r2.first]
        _tmp << [_r2.last,right]
      else # 4.range与区间存在交集时
        _r2.first > left ? _tmp << [left,_r2.first] : _tmp << [_r2.last,right]
      end
    end

    @rl = _tmp
  end 
  
  def print
    puts @rl.map{|m| "[" + m.join(',') + ')'}.join
  end
end

rl = RangeList.new 

puts "=========== start add"
# 前提：保证输入有序
rl.add([1, 5]) 
rl.print # Should display: [1, 5) 

rl.add([10, 20]) 
rl.print # Should display: [1, 5) [10, 20)

rl.add([20, 20]) 
rl.print # Should display: [1, 5) [10, 20)

rl.add([20, 21]) 
rl.print # Should display: [1, 5) [10, 21) 

rl.add([2, 4]) 
rl.print # Should display: [1, 5) [10, 21) 

rl.add([3, 8]) 
rl.print # Should display: [1, 8) [10, 21) 

puts "=========== start remove"

rl.remove([10, 10]) 
rl.print # Should display: [1, 8) [10, 21) 

rl.remove([10, 11]) 
rl.print # Should display: [1, 8) [11, 21) 

rl.remove([15, 17]) 
rl.print # Should display: [1, 8) [11, 15) [17, 21) 

rl.remove([3, 19]) 
rl.print # Should display: [1, 3) [19, 21)